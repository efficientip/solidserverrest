* in a virtual env with a clone of the repo, install requirements
* install twine: ```pip install twine build```
* change setup.py with appropriate version tag
* prepare package: ```python -m build```
* push to pypi: ```twine upload dist/*```
