# Method dhcp_delegated_prefix6_delete

## Description
	Delete a delegated prefix

## Mandatory Input Parameters

## Input Parameters
	* delagatedprefix6_id: type: >0
	* dhcp6_id: type: >0
	* addr: type: ipv6_addr
	* prefix: type: >0
	* time: type: >=0
	* end_time: type: >=0
	* client_duid: type: string
	* name: type: string
	* validate_warnings: type: enum, values: accept

## Returned Values
	* ret_oid
	* errno
	* errmsg
	* msg
	* severity
	* parameters
	* param_format
	* param_value
	* ret_code


*this file is automatically generated* - date: 19 Aug 2024 10:17