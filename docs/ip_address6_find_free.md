# Method ip_address6_find_free

## Description
This service allows you to list the 10 first free IPv6 addresses.
To execute this service, users must be granted the permission to use it. The rows returned to
the user running the service depend on the resources granted to the group they belong to.

## Mandatory Input Parameters
(subnet6_id || pool6_id || parent_subnet6_id)

## Input Parameters
 * subnet6_id: The database identifier (ID) of the IPv6 network, a unique numeric key value automatically incremented when you add an IPv6 network. Use the ID to specify the IPv6 network of your choice.
 * parent_subnet6_id: The database identifier (ID) of the parent IPv6 network. Use the ID to specify the parent IPv6 network of your choice.
 * pool6_id: The database identifier (ID) of the IPv6 pool, a unique numeric key value automatically incremented when you add an IPv6 pool. Use the ID to specify the IPv6 pool of your choice.
 * max_find: The maximum number of IPv6 addresses to be returned by the service. You can use it to
return more than 10 results.
* begin_addr: The first IPv6 address of the range of addresses where you are looking for free IP addresses.
* end_addr: The last IPv6 address of the range of addresses where you are looking for free IP addresses.
* pool6_class_name: The name of the class applied to the IPv6 pool the IP addresses you are looking for belong to. You must specify the class file directory, e.g. my_directory/my_class.class . You cannot use the classes global and default, they are reserved by the system.
* subnet6_class_name: The name of the class applied to the IPv6 network the IP addresses you are looking for belong to. You must specify the class file directory, e.g. my_directory/my_class.class . You cannot use the classes global and default, they are reserved by the system.

## Returned Values
 * ip6_addr: The IPv6 address itself.
 * hostaddr6: The IP address.
 * site_id: The database identifier (ID) of the space the object belongs to, a unique numeric key valueautomatically incremented when you add a space.
 * site_name: The name of the space the object belongs to.
 * subnet_id: The database identifier (ID) of the IPv6 network the object belongs to.
 * subnet6_name The name of the IPv6 network the object belongs to.
 * pool6_id: The database identifier (ID) of the IPv6 pool the object belongs to.
 * pool6_name: The name of the IPv6 pool the object belongs to.

*this file is manually generated* - date: 19 Feb 2024 13:41