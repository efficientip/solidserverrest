# Method ip_address_find_free

## Description
This service allows you to list the 10 first free IPv4 addresses.
To execute this service, users must be granted the permission to use it. The rows returned to
the user running the service depend on the resources granted to the group they belong to.

## Mandatory Input Parameters
(subnet_id || pool_id || parent_subnet_id)

## Input Parameters

 * subnet_id: The database identifier (ID) of the IPv4 network, a unique numeric key value automatically incremented when you add an IPv4 network. Use the ID to specify the IPv4 network of your choice.

 * parent_subnet_id: The database identifier (ID) of the parent IPv4 network. Use the ID to specify the parent IPv4 network of your choice.

 * pool_id: The database identifier (ID) of the IPv4 pool, a unique numeric key value automatically incremented when you add an IPv4 pool. Use the ID to specify the IPv4 pool of your choice.

 * max_find: The maximum number of IPv4 addresses to be returned by the service. You can use it to
return more than 10 results.


 * begin_addr: The first IPv4 address of the range of addresses where you are looking for free IP addresses.
 * end_addr: The last IPv4 address of the range of addresses where you are looking for free IP addresses.
 * pool_class_name: The name of the class applied to the IPv4 pool the IP addresses you are looking for belong to. You must specify the class file directory, e.g. my_directory/my_class.class . You cannot use the classes global and default, they are reserved by the system.
 * subnet_class_name: The name of the class applied to the IPv4 network the IP addresses you are looking for belong to. You must specify the class file directory, e.g. my_directory/my_class.class . You cannot use the classes global and default, they are reserved by the system.

## Returned Values
 * ip_addr The IPv4 address itself, in hexadecimal format.
 * hostaddr: The human readable version of the parameter ip_addr.
 * site_id: The database identifier (ID) of the space the object belongs to, a unique numeric key value automatically incremented when you add a space.
 * site_name: The name of the space the object belongs to.
 * subnet_id: The database identifier (ID) of the IPv4 network the object belongs to.
subnet_name The name of the IPv4 network the object belongs to. Default indicates that the network the object belongs to is an orphan network.
* pool_id: The database identifier (ID) of the IPv4 pool the object belongs to.
* pool_name: The name of the IPv4 pool the object belongs to.

*this file is manually generated* - date: 19 Feb 2024 13:41