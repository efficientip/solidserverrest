# Method ip_alias_create

## Description
	Add/modify an IP address alias

## Mandatory Input Parameters
	(ip_name && (ip_id || (hostaddr && (site_id || site_name))))

## Input Parameters
	* site_id: type: >0
	* site_name: type: string, max length: 128
	* ip_id: type: >0, can be empty: true
	* ip_name_id: type: >0, can be empty: true
	* ip_name: type: string
	* name: type: string
	* ip_name_type: type: enum, values: A,a,CNAME,cname
	* ip_addr: type: ipv4_addr
	* hostaddr: type: ipv4_addr
	* add_flag: type: enum, default: new_edit, values: new_edit,new_only,edit_only
	* validate_warnings: type: enum, values: accept

## Returned Values
	* ret_oid
	* errno
	* errmsg
	* msg
	* severity
	* parameters
	* param_format
	* param_value
	* ret_code


*this file is automatically generated* - date: 19 Aug 2024 10:17