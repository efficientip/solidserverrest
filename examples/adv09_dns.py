#!/usr/bin/python
# -*-coding:Utf-8 -*
#
# connects to a SOLIDserver, create dns server, zone and records
#
##########################################################

import logging
import pprint
import os, sys
import uuid

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

from SOLIDserverRest import *
from SOLIDserverRest import adv as sdsadv

logging.basicConfig(format='[%(filename)s:%(lineno)d] %(levelname)s: %(message)s',
                    level=logging.INFO)

# configuration - to be adapted
SDS_HOST = "192.168.24.230"
SDS_LOGIN = "ipmadmin"
SDS_PWD = "admin"

logging.info("create a connection to the SOLIDserver")

sds = sdsadv.SDS(ip_address=SDS_HOST,
                 user=SDS_LOGIN,
                 pwd=SDS_PWD)

try:
    sds.connect(method="native")
except SDSError as e:
    logging.error(e)
    exit()

logging.info(sds)

# --------------------------
logging.info("create DNS SMARTArchitecture server")
dns = sdsadv.DNS(name="test-{}.labo".format(str(uuid.uuid4())[0:8]),
                 sds=sds)
dns.set_type('vdns', 'single')
dns.create()

logging.info(dns)

# --------------------------
logging.info("create DNS zone")
dns_zone = sdsadv.DNS_zone(sds=sds,
                           name="test-{}.labo".format(str(uuid.uuid4())[0:8]))

dns_zone.set_dns(dns)
dns_zone.set_type(dns_zone.TYPE_MASTER)
dns_zone.create()

logging.info(dns_zone)

# --------------------------
logging.info("create DNS record")
name = "{}.{}".format(str(uuid.uuid4())[0:8],
                          dns_zone.name)
dns_rr = sdsadv.DNS_record(sds, name)
dns_rr.set_zone(dns_zone)
dns_rr.set_type('A', ip='127.1.2.3')
dns_rr.create()

logging.info(dns_rr)
dns_rr.delete()

dns_rr = sdsadv.DNS_record(sds, name)
dns_rr.set_zone(dns_zone)
dns_rr.set_type('TXT', txt='test')
dns_rr.create()

logging.info(dns_rr)
dns_rr.delete()

dns_rr = sdsadv.DNS_record(sds, name)
dns_rr.set_zone(dns_zone)
dns_rr.set_type('MX', priority=10, target="foo.bar")
dns_rr.create()

logging.info(dns_rr)
dns_rr.delete()

some_values = {
    "order":10,
    "preference":10,
    "flags":"A",
    "services": "services-value",
    "regex":"",
    "replace":"replace-value",
    "ttl":300      
}
test_naptr = sdsadv.NAPTRRecord(
    sds=sds,
    server=dns,
    zone=dns_zone,
    name="some-name",
    **some_values
)

# create
test_naptr.create()
logging.info(test_naptr)

# fetch created in new instance, use names instead of instances
fetched_existing = sdsadv.NAPTRRecord(
    sds=sds,
    server=dns.name,
    zone=dns_zone.name,
    name="some-name",
    fetch_existing=True
)

# update a value
fetched_existing.order = 5
fetched_existing.update()

logging.info(fetched_existing)

# delete
fetched_existing.delete()


some_values = {
    "ttl":300,
    "text": "text value for this record"
}
test_txt = sdsadv.TXTRecord(
    sds=sds,
    server=dns,
    zone=dns_zone,
    name="txt-record",
    **some_values
)
test_txt.create()
logging.info(test_txt)

# --------------------------
logging.info("cleaning")
dns_zone.delete()
dns.delete()

del sds
