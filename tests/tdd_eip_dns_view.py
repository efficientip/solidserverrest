# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2024-01-30 14:20:05 alex>
#

"""test file for DNS records

* test_create_dns_rr_object
* test_dns_rr_A_smart
* test_dns_rr_20xA_smart
* test_dns_rr_A_srv
* test_dns_rr_10xA_srv
* test_dns_rr_200xA_async_srv
* test_dns_rr_special_smart

start DNS in docker for test:
 sudo iptables -P FORWARD ACCEPT
 sudo sysctl net.ipv4.conf.all.forwarding=1
 # docker run --rm -d eip-package-dns
 # see docker-compose in tests

"""

import logging
import sys
import uuid
import datetime
import time
import random

from SOLIDserverRest.Exception import (SDSInitError, SDSRequestError,
                                       SDSAuthError, SDSError, SDSDeviceIfError,
                                       SDSEmptyError, SDSSpaceError,
                                       SDSDeviceError, SDSDeviceNotFoundError,
                                       SDSDNSError,
                                       SDSDNSAlreadyExistingError,
                                       SDSDNSCredentialsError)

from .context import sdsadv
from .context import _connect_to_sds
from .adv_basic import *
from .dns_requester import DNSRequester

try:
    from tests.data_sample import *
except:
    from .data_sample import *


# ZONE_NAME = "tddz-12345678.labo"
# VIEW_NAME = "tddv-123.labo"
# SMART_NAME = "tdds-12345678.labo"

ZONE_NAME = f"tdd-z{uuid.uuid4()}.labo"
VIEW_NAME = f"tdd-v{uuid.uuid4()}.labo"
SMART_NAME = f"tdd-s{uuid.uuid4()}.labo"

ACTIVE_TESTING = False
DNS = DNSRequester()
DNS.setServer("192.168.24.230")

# -------------------------------------------------------


def _dns_create_smart():
    """create a dns server or return existing one"""

    dns_name = DNS_SMART_NAME

    sds = _connect_to_sds()
    dns = sdsadv.DNS(name=dns_name, sds=sds)
    dns.set_type(newtype='vdns', vdns_arch='single')
    # dns.set_ipv4(DNS_SRV01_IP)
    dns.set_ipm_credentials('admin', 'admin')
    try:
        dns.create()
    except SDSError:
        dns.refresh()

    return dns


def _dns_create_smart(sds, random_name=False):
    """create a dns smart or return existing one"""

    if random_name:
        dns_name = "tdd-{}.labo".format(str(uuid.uuid4())[0:8])
    else:
        dns_name = SMART_NAME

    dns = sdsadv.DNS(name=dns_name, sds=sds)
    dns.set_type('vdns', 'single')
    try:
        dns.create()
    except SDSError:
        dns.refresh()

    return dns


def _dns_create_zone(sds, dns=None, random_name=False):
    # coverage
    if random_name:
        name = "tdd-{}.labo".format(str(uuid.uuid4())[0:8])
    else:
        name = ZONE_NAME

    try:
        dns_zone = sdsadv.DNS_zone()
        dns_zone.create()
        assert None, "no connection"
    except SDSInitError:
        pass

    dns_zone = sdsadv.DNS_zone(sds=sds, name=name)

    dns_zone.set_dns(dns)
    dns_zone.set_type(dns_zone.TYPE_MASTER)
    try:
        dns_zone.create()
    except SDSDNSError:
        dns_zone.refresh()

    return dns_zone
# -------------------------------------------------------


def test_create_dns_view_object():
    dns_view = sdsadv.DNS_view()
    dns_view = sdsadv.DNS_view(class_params={'key': 'value'})


def test_create_dns_view_bad_dns():
    sds = _connect_to_sds()
    name = str(uuid.uuid4())

    dns_view = sdsadv.DNS_view(sds=sds, name=name)

    try:
        dns_view.set_dns("foo")
        assert None, "set bad dns should raise error"
    except:
        pass

    try:
        dns_view.refresh()
        assert None, "set bad dns should raise error"
    except:
        pass


def test_create_dns_view_bad_sds():
    name = str(uuid.uuid4())

    try:
        dns_view = sdsadv.DNS_view(sds="foo", name=name)
        assert None, "set bad SDS should raise error"
    except:
        pass

    try:
        dns_view.refresh()
        assert None, "bad SDS should raise error"
    except:
        pass


def test_refresh_dns_view_bad_sds():
    name = str(uuid.uuid4())

    dns_view = sdsadv.DNS_view(name=name)
    try:
        dns_view.refresh()
        assert None, "bad SDS should raise error"
    except:
        pass


def test_dns_view_create_in_smart():
    sds = _connect_to_sds()

    dns_srv = _dns_create_smart(sds)

    name = str(uuid.uuid4())[0:8]
    # name = VIEW_NAME

    dns_view = sdsadv.DNS_view(sds, name)

    # create view without server, shoudl raise error
    try:
        dns_view.create(dns_srv)
        assert None, "view creation, should fail"
    except SDSDNSError as e:
        pass

    # create view with DNS server
    dns_view.set_dns(dns_srv)
    try:
        dns_view.create(dns_srv)
    except SDSDNSError as e:
        logging.error(e)
        assert None, "view creation, should fail"

    dns_view.delete()
    dns_srv.delete()


def test_refresh_existing_view():
    # init
    sds = _connect_to_sds()
    dns_srv = _dns_create_smart(sds)

    # create the zone
    name = str(uuid.uuid4())
    dns_view = sdsadv.DNS_view(sds=sds,
                               name=name,
                               class_params={'key': 'value'})

    dns_view.set_dns(dns_srv)

    logging.debug(str(dns_view))
    dns_view.create()

    dns_view.refresh()

    sdns1 = str(dns_view)

    dns_view2 = sdsadv.DNS_view(sds=sds,
                                name=name,
                                class_params={'key': 'value'})
    dns_view2.set_dns(dns_srv)

    try:
        dns_view2.refresh()
        sdns2 = str(dns_view2)

        if sdns1 != sdns2:
            logging.info(sdns1)
            logging.info(sdns2)
            assert None, "refresh on view not working"
    except SDSError:
        dns_view.delete()
        assert None, "refresh on existant zone should not fail"

    dns_view.delete()
    dns_srv.delete()


def test_refresh_nonexisting_view():
    # init
    sds = _connect_to_sds()
    dns_srv = _dns_create_smart(sds)

    # create the zone
    name = str(uuid.uuid4())
    dns_view = sdsadv.DNS_view(sds=sds, name=name)
    dns_view.set_dns(dns_srv)

    try:
        dns_view.refresh()
        assert None, "refresh on nonexistant zone should not fail"
    except SDSError:
        None

    dns_srv.delete()


def test_delete_nonexisting_view():
    # init
    sds = _connect_to_sds()
    dns_srv = _dns_create_smart(sds)

    # create the zone
    name = str(uuid.uuid4())
    dns_view = sdsadv.DNS_view(sds=sds, name=name)

    try:
        dns_view.sds = None
        dns_view.delete()
        assert None, "delete on nonexistant zone should not fail"
    except SDSError:
        None

    dns_view = sdsadv.DNS_view(sds=sds, name=name)
    dns_view.set_dns(dns_srv)

    try:
        dns_view.delete()
        assert None, "delete on nonexistant zone should not fail"
    except SDSError:
        None
